// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

extern crate deb_version;
extern crate ruscan;
extern crate url;

mod extractor;

use ruscan::WatchFile;
use std::process::exit;
use url::Url;

fn main() {
    if let Ok(watch_files) = WatchFile::from_file("debian/watch") {
        for watch in watch_files {
            let mut urls = extractor::extract_hrefs_from_url(
                watch.url.as_ref().expect("Watch file did not contain an url"),
                &watch.pattern.expect("Watch file did not contain a pattern")
            ).expect("Failed to extract any urls. Check your internet connection and watch file. Error: {}");

            let url: String;
            urls.sort();
            match urls.last() {
                Some(l) => {
                    if l.starts_with('/') {
                        let mut watch_url = Url::parse(watch.url.unwrap().as_str())
                            .expect("Url from watch file is not parsable");
                        watch_url.set_path(l);
                        url = watch_url.to_string()
                    } else {
                        url = l.to_string()
                    }
                }
                None => {
                    println!("Failed to extract any urls. Check your watch file.");
                    exit(1);
                }
            };
            println!("{}", url);
        }
    } else {
        println!("Can't read debian/watch");
        exit(1);
    }
}
