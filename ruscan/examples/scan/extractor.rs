// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

extern crate reqwest;
extern crate scraper;

use regex::Regex;
use scraper::{Html, Selector};

#[cfg(test)]
use std::fs::File;
#[cfg(test)]
use std::io::prelude::*;
#[cfg(test)]
use std::path::Path;
#[cfg(test)]
use std::process::Command;

fn download_html(url: &str) -> Result<String, reqwest::Error> {
    let body = reqwest::blocking::get(url)?;

    body.text()
}

fn extract_matching_hrefs(html: &str, pattern: &str) -> Vec<String> {
    let document = Html::parse_document(html);

    let selector = Selector::parse("a").unwrap();

    let mut matches: Vec<String> = Vec::new();

    for element in document.select(&selector) {
        match element.value().attr("href") {
            Some(href) => {
                if Regex::new(pattern).unwrap().is_match(href) {
                    matches.push(href.to_owned())
                }
            }
            None => continue,
        }
    }

    matches
}

pub fn extract_hrefs_from_url(url: &str, pattern: &str) -> Result<Vec<String>, reqwest::Error> {
    match download_html(url) {
        Ok(html) => Ok(extract_matching_hrefs(&html, pattern)),
        Err(e) => Err(e),
    }
}

#[test]
fn match_github() {
    let mut github_tags = String::new();
    File::open(Path::new("test_files/tags"))
        .unwrap()
        .read_to_string(&mut github_tags);

    let hrefs = extract_matching_hrefs(&github_tags, r".*/v?(\d\S+)\.tar\.gz");
    assert!(!hrefs.is_empty());
    assert_eq!(
        hrefs,
        vec![
            "/minetest/minetest/archive/5.1.1.tar.gz",
            "/minetest/minetest/archive/5.1.0.tar.gz",
            "/minetest/minetest/archive/5.0.1.tar.gz",
            "/minetest/minetest/archive/5.0.0.tar.gz",
            "/minetest/minetest/archive/0.4.17.1.tar.gz",
            "/minetest/minetest/archive/0.4.17.tar.gz",
            "/minetest/minetest/archive/0.4.16.tar.gz",
            "/minetest/minetest/archive/0.4.15.tar.gz",
            "/minetest/minetest/archive/0.4.14.tar.gz",
            "/minetest/minetest/archive/0.4.13.tar.gz"
        ]
    );
}

// No idea whether having a test depend on the internet connection is a good idea.
#[test]
fn download_local_html() {
    Command::new("busybox")
        .args(&["busybox", "httpd", "-p", "8080", "-h", "test_files"])
        .spawn()
        .expect("Failed to start web server");
    let result = download_html("http://localhost:8080/tags");

    assert!(result.is_ok());
    assert!(!result.unwrap().is_empty());

    Command::new("killall")
        .args(&["busybox"])
        .spawn()
        .expect("Failed to stop web server");
}

#[test]
fn extract_hrefs_from_local_url() {
    Command::new("busybox")
        .args(&["busybox", "httpd", "-p", "8081", "-h", "test_files"])
        .spawn()
        .expect("Failed to start web server");
    let result = extract_hrefs_from_url("http://localhost:8081/tags", r".*/v?(\d\S+)\.tar\.gz");

    assert!(result.is_ok());
    let hrefs = result.unwrap();
    assert!(!hrefs.is_empty());
    assert_eq!(
        hrefs,
        vec![
            "/minetest/minetest/archive/5.1.1.tar.gz",
            "/minetest/minetest/archive/5.1.0.tar.gz",
            "/minetest/minetest/archive/5.0.1.tar.gz",
            "/minetest/minetest/archive/5.0.0.tar.gz",
            "/minetest/minetest/archive/0.4.17.1.tar.gz",
            "/minetest/minetest/archive/0.4.17.tar.gz",
            "/minetest/minetest/archive/0.4.16.tar.gz",
            "/minetest/minetest/archive/0.4.15.tar.gz",
            "/minetest/minetest/archive/0.4.14.tar.gz",
            "/minetest/minetest/archive/0.4.13.tar.gz"
        ]
    );
    Command::new("killall")
        .args(&["busybox"])
        .spawn()
        .expect("Failed to stop web server");
}
