// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

extern crate gtmpl;
extern crate serde;
extern crate serde_yaml;

use gtmpl::error::TemplateError;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::error::Error;
use std::fmt::Display;
use std::fmt::Formatter;
use std::path::Path;

#[derive(Debug)]
pub enum DebosError {
    Template(Box<TemplateError>),
    Yaml(serde_yaml::Error),
    IO(std::io::Error),
}

impl Display for DebosError {
    fn fmt(&self, formatter: &mut Formatter) -> Result<(), std::fmt::Error> {
        match self {
            DebosError::Template(t) => t.fmt(formatter),
            DebosError::Yaml(y) => y.fmt(formatter),
            DebosError::IO(io) => io.fmt(formatter),
        }
    }
}

impl Error for DebosError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        match self {
            DebosError::Template(t) => Some(t),
            DebosError::Yaml(y) => Some(y),
            DebosError::IO(io) => Some(io),
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, Default)]
#[serde(rename_all = "kebab-case")]
#[serde(deny_unknown_fields)]
pub struct Partition {
    pub name: String,
    pub fs: String,
    pub start: String,
    pub end: String,
    pub features: Option<String>,
    pub flags: Option<Vec<String>>,
    pub fsck: Option<bool>,
}

#[derive(Serialize, Deserialize, Clone, Debug, Default)]
#[serde(rename_all = "kebab-case")]
//#[serde(deny_unknown_fields)]
pub struct Mountpoint {
    pub mountpoint: String,
    pub partition: String,
    pub options: Option<String>,
    pub buildtime: Option<String>,
}

#[derive(Serialize, Deserialize, Clone, Debug, Default)]
pub struct AptAction {
    pub recommends: Option<bool>,
    pub unauthenticated: Option<bool>,
    pub packages: Vec<String>,
    pub update: Option<bool>,
}

impl AptAction {
    pub fn new(packages: Vec<String>) -> Self {
        Self {
            recommends: None,
            unauthenticated: None,
            update: None,
            packages,
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, Default)]
pub struct DebootstrapAction {
    pub mirror: Option<String>,
    pub suite: String,
    pub components: Option<Vec<String>>,
    pub variant: Option<String>,
    pub keyring_package: Option<String>,
    pub keyring_file: Option<String>,
    pub merged_usr: Option<bool>,
}

impl DebootstrapAction {
    pub fn new(suite: String) -> Self {
        Self {
            mirror: None,
            suite,
            components: None,
            variant: None,
            keyring_package: None,
            keyring_file: None,
            merged_usr: None,
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, Default)]
pub struct DownloadAction {
    pub url: String,
    pub name: String,
    pub filename: Option<String>,
    pub unpack: Option<bool>,
    pub compression: Option<String>,
}

impl DownloadAction {
    pub fn new(name: String, url: String) -> Self {
        Self {
            url,
            name,
            filename: None,
            unpack: None,
            compression: None,
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, Default)]
pub struct FilesystemDeployAction {
    pub setup_fstab: Option<bool>,
    pub setup_kernel_cmdline: Option<bool>,
    pub append_kernel_cmdline: Option<String>,
}

#[derive(Serialize, Deserialize, Clone, Debug, Default)]
pub struct ImagePartitionAction {
    pub imagename: String,
    pub imagesize: String,
    pub partitiontype: String,
    #[serde(rename(serialize = "gpg_gap", deserialize = "gpg_gap"))]
    pub gpt_gap: Option<i64>,
    pub partitions: Vec<Partition>,
    pub mountpoints: Vec<Mountpoint>,
}

#[derive(Serialize, Deserialize, Clone, Debug, Default)]
pub struct OstreeCommitAction {
    pub repository: String,
    pub branch: String,
    pub subject: Option<String>,
    pub collection_id: Option<String>,
    pub ref_binding: Option<Vec<String>>,
    pub metadata: Option<HashMap<String, String>>,
}

#[derive(Serialize, Deserialize, Clone, Debug, Default)]
pub struct OstreeDeployAction {
    pub repository: String,
    #[serde(rename(serialize = "remote_repository", deserialize = "remote_repository"))]
    pub remote_repository: String,
    pub branch: String,
    pub os: String,
    pub tls_client_cert_path: Option<String>,
    pub tls_client_key_path: Option<String>,
    pub setup_fstab: Option<bool>,
    pub setup_kernel_cmdline: Option<bool>,
    pub append_kernel_cmdline: Option<String>,
    pub collection_id: String,
}

#[derive(Serialize, Deserialize, Clone, Debug, Default)]
pub struct OverlayAction {
    pub origin: Option<String>,
    pub source: String,
    pub destination: Option<String>,
}

#[derive(Serialize, Deserialize, Clone, Debug, Default)]
pub struct PackAction {
    pub file: String,
    pub compression: String,
}

#[derive(Serialize, Deserialize, Clone, Debug, Default)]
pub struct RawAction {
    pub origin: String,
    pub source: String,
    pub offset: Option<i64>,
    pub partition: Option<String>,
}

#[derive(Serialize, Deserialize, Clone, Debug, Default)]
pub struct RecipeAction {
    pub recipe: String,
    pub variables: Option<HashMap<String, String>>,
}

#[derive(Serialize, Deserialize, Clone, Debug, Default)]
pub struct RunAction {
    pub chroot: Option<bool>,
    pub postrocess: Option<bool>,
    pub script: Option<String>,
    pub command: Option<String>,
    pub label: Option<String>,
}

impl RunAction {
    pub fn new(command: String) -> Self {
        Self {
            command: Some(command),
            label: None,
            script: None,
            postrocess: None,
            chroot: None,
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, Default)]
pub struct UnpackAction {
    pub origin: Option<String>,
    pub file: String,
    pub compression: Option<String>,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(rename_all = "kebab-case")]
#[serde(deny_unknown_fields)]
#[serde(tag = "action")]
pub enum DebosAction {
    #[serde(rename = "apt")]
    Apt(AptAction),
    #[serde(rename = "debootstrap")]
    Debootstrap(DebootstrapAction),
    #[serde(rename = "download")]
    Download(DownloadAction),
    #[serde(rename = "filesystem-deploy")]
    FilesystemDeploy(FilesystemDeployAction),
    #[serde(rename = "image-partition")]
    ImagePartition(ImagePartitionAction),
    #[serde(rename = "ostree-commit")]
    OstreeCommit(OstreeCommitAction),
    #[serde(rename = "ostree-deploy")]
    OstreeDeploy(OstreeDeployAction),
    #[serde(rename = "overlay")]
    Overlay(OverlayAction),
    #[serde(rename = "pack")]
    Pack(PackAction),
    #[serde(rename = "raw")]
    Raw(RawAction),
    #[serde(rename = "recipe")]
    Recipe(RecipeAction),
    #[serde(rename = "run")]
    Run(RunAction),
    #[serde(rename = "unpack")]
    Unpack(UnpackAction),
}

impl From<AptAction> for DebosAction {
    fn from(action: AptAction) -> Self {
        DebosAction::Apt(action)
    }
}

impl From<DebootstrapAction> for DebosAction {
    fn from(action: DebootstrapAction) -> Self {
        DebosAction::Debootstrap(action)
    }
}

impl From<DownloadAction> for DebosAction {
    fn from(action: DownloadAction) -> Self {
        DebosAction::Download(action)
    }
}

impl From<FilesystemDeployAction> for DebosAction {
    fn from(action: FilesystemDeployAction) -> Self {
        DebosAction::FilesystemDeploy(action)
    }
}

impl From<ImagePartitionAction> for DebosAction {
    fn from(action: ImagePartitionAction) -> Self {
        DebosAction::ImagePartition(action)
    }
}

impl From<OstreeCommitAction> for DebosAction {
    fn from(action: OstreeCommitAction) -> Self {
        DebosAction::OstreeCommit(action)
    }
}

impl From<OverlayAction> for DebosAction {
    fn from(action: OverlayAction) -> Self {
        DebosAction::Overlay(action)
    }
}

impl From<OstreeDeployAction> for DebosAction {
    fn from(action: OstreeDeployAction) -> Self {
        DebosAction::OstreeDeploy(action)
    }
}

impl From<PackAction> for DebosAction {
    fn from(action: PackAction) -> Self {
        DebosAction::Pack(action)
    }
}

impl From<RawAction> for DebosAction {
    fn from(action: RawAction) -> Self {
        DebosAction::Raw(action)
    }
}

impl From<RecipeAction> for DebosAction {
    fn from(action: RecipeAction) -> Self {
        DebosAction::Recipe(action)
    }
}

impl From<RunAction> for DebosAction {
    fn from(action: RunAction) -> Self {
        DebosAction::Run(action)
    }
}

impl From<UnpackAction> for DebosAction {
    fn from(action: UnpackAction) -> Self {
        DebosAction::Unpack(action)
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, Default)]
#[serde(deny_unknown_fields)]
pub struct DebosRecipe {
    architecture: String,
    actions: Vec<DebosAction>,
}

impl DebosRecipe {
    pub fn architecture(&self) -> &String {
        &self.architecture
    }

    pub fn add_action(&mut self, action: DebosAction) {
        self.actions.push(action)
    }

    pub fn insert_action(&mut self, index: usize, action: DebosAction) {
        self.actions.insert(index, action)
    }

    pub fn actions(&self) -> &Vec<DebosAction> {
        &self.actions
    }

    pub fn set_actions(&mut self, actions: Vec<DebosAction>) {
        self.actions = actions
    }

    pub fn from_template(
        template: &str,
        values: HashMap<String, String>,
    ) -> Result<DebosRecipe, DebosError> {
        let yaml = gtmpl::template(template, values).map_err(Box::from).map_err(DebosError::Template)?;
        serde_yaml::from_str::<DebosRecipe>(&yaml).map_err(DebosError::Yaml)
    }

    pub fn from_template_file(
        path: &Path,
        values: HashMap<String, String>,
    ) -> Result<DebosRecipe, DebosError> {
        let input = std::fs::read_to_string(path);
        match input {
            Err(e) => Err(DebosError::IO(e)),
            Ok(o) => DebosRecipe::from_template(&o, values),
        }
    }

    pub fn to_yaml(&self) -> Result<String, serde_yaml::Error> {
        serde_yaml::to_string(self)
    }
}

#[cfg(test)]
mod tests {
    use crate::DebosAction;
    use crate::DebosRecipe;
    use crate::*;
    use std::fs::File;
    #[test]
    fn single_file() {
        let stream = File::open("recipe.yaml").unwrap();
        let mut recipe: DebosRecipe = serde_yaml::from_reader(&stream).unwrap();
        assert_eq!(recipe.actions.len(), 14);

        let mut apt_action = crate::AptAction::new(vec!["plasma-desktop".to_owned()]);
        apt_action.packages = vec![
            "bash".to_owned(),
            "network-manager".to_owned(),
            "plasma-desktop".to_owned(),
        ];
        apt_action.recommends = Some(true);
        apt_action.unauthenticated = Some(false);

        let mut run_action = crate::RunAction::new("echo hi".to_owned());
        run_action.label = Some("saying hi".to_owned());
        recipe.add_action(DebosAction::from(apt_action));
        recipe.add_action(DebosAction::from(run_action));
        assert_eq!(recipe.actions.len(), 16);
        assert_eq!(
            match &recipe.actions[15] {
                DebosAction::Run(a) => &a.command,
                _ => unreachable!(),
            },
            &Some("echo hi".to_owned())
        );

        println!("{}", serde_yaml::to_string(&recipe).unwrap())
    }
}
