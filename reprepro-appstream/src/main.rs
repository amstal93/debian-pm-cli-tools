// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

use std::path::PathBuf;

#[macro_use]
extern crate serde;

mod asgen;
mod asgen_config;

use asgen_config::AsgenConfig;

fn env_var(key: &str) -> String {
    std::env::var(key).unwrap_or_else(|_| panic!("Missing required environment variable {}", key))
}

fn abs_path(path: String) -> String {
    PathBuf::from(path)
        .canonicalize()
        .expect("directory does not exist")
        .to_string_lossy()
        .to_string()
}

fn main() {
    let mut args = std::env::args();
    // Skip program name
    args.next();

    let dists_dir = args.next();
    let new_packages = args.next();
    let old_packages = args.next();
    let operation = args.next();

    if dists_dir.is_none()
        || new_packages.is_none()
        || old_packages.is_none()
        || operation.is_none()
    {
        eprintln!("This program is a reprepro hook, don't call it directly");
        std::process::exit(1);
    }

    if let Some(dists_dir) = dists_dir {
        if let Some(new_packages) = new_packages {
            let dist = dists_dir.split('/').last().unwrap();
            let component = new_packages.split('/').next().unwrap();

            println!("Generating appstream metadata for {}/{}", dist, component);

            if let Some(op) = operation {
                if op == "new" {
                    println!("No files for me to parse exist yet, see you next time");
                    std::process::exit(0)
                }
            }

            let conf_dir = env_var("REPREPRO_CONFIG_DIR");
            let archive_dir: String = abs_path(env_var("REPREPRO_OUT_DIR"));

            let config = AsgenConfig::generate(&PathBuf::from(conf_dir), &archive_dir);
            asgen::run(config, dist, component);
        }
    }
}
