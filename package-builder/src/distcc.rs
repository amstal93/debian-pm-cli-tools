// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

use crate::config::{self, Config};
use crate::container::{Container, ExecOptions, RunOptions};
use crate::images;
use crate::io_check;
use crate::println_status;

use std::io::Result as IoResult;

///
/// Distcc container management
///
pub struct Distcc {
    container: Container,
}

impl Distcc {
    fn create_or_start_container() -> IoResult<Container> {
        if let Some(distcc_container_config) = &Config::global().distcc_container {
            if let Ok(distcc_container) =
                Container::from_existing_id(&distcc_container_config.id, false)
            {
                return Ok(distcc_container);
            }
        }

        let distcc_container = Container::start_with_options(
            &images::native_base_image("testing"),
            RunOptions::default(),
        )?;

        // Store new container id in config
        Config::global().distcc_container = Some(config::Container {
            id: distcc_container.id().to_string(),
        });

        Ok(distcc_container)
    }

    ///
    /// Create or start an existing distcc container
    /// If it exists, The ID will be picked up from the config
    /// Otherwise, a new container will be spawned and necessary
    /// packages will be installed in it
    ///
    pub fn start(build_container: &Container, architecture: &str) -> IoResult<Distcc> {
        let distcc_container = Distcc::create_or_start_container()?;

        println_status!("Installing cross toolchains");

        io_check!(distcc_container.exec_with_options(
            ExecOptions::default().user("root"),
            &["apt-get", "update"]
        ));
        io_check!(distcc_container.exec_with_options(
            ExecOptions::default().user("root"),
            &[
                "apt-get",
                "install",
                "-y",
                "--no-install-recommends",
                "distcc",
                "build-essential",
                &format!("crossbuild-essential-{}", architecture)
            ]
        ));

        let build_ip_addr = &build_container.inspect().network_settings.ip_address;
        io_check!(distcc_container.exec(&["distccd", "--daemon", "--allow", build_ip_addr]));

        Ok(Distcc {
            container: distcc_container,
        })
    }

    ///
    /// Returns the ip address of the distcc container
    ///
    pub fn ip_address(&self) -> &str {
        &self.container.inspect().network_settings.ip_address
    }
}
