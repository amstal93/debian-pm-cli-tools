# debian-pm cli tools

This is a collection of command line tools developed for the debian-pm project.
The aim is to write them as generically as possible so they can be used in other projects.

## package-builder

Tool for development of packages.
* Builds packages and their dependencies in a containerized environment (currently docker)
* Runs on any distribution that provides rust and docker (not tied to running on debian)
* Can build all packages referenced by a debos recipe to allow building images without having a hosted repository.
* Makes use of distcc for quick builds for foreign architectures
* Makes use of ccache for quick subsequent builds

**status**: early, but usable

## package-generator

Question based way to generate more useful package templates than dh_make.
For simple packages the generated packaging will already be enough to build a working package.

**status**: early, but usable

## package-scanner

Extremely simple repository generator.
* Doesn't maintain its own directory structure
* generates a `Packages` file referencing the packages found in a directory recursively
* used internally by package-builder

**status**: finished

## reprepro-appstream

reprepro hook for generating appstream metadata.
It detects almost all settings from the existing reprepro configuration, so creating an `asgen-config.json` file is not necessary.

The reprepro configuration directory must contain a file named `conf/appstream` containing a `BaseUrl` entry in the first parargaph.
For example:
```
BaseUrl: https://jbb.ghsq.ga/debpm/
```

The hook can be called by reprepro by adding the following to `conf/distributions`:
```
DebIndices: Packages Release . .gz /path/to/reprepro-appstream
```

**status**: finished

## reprepro-server

HTTP API for reprepro.
Allows easily hosting a repository that can be used for submitting packages from the CI.
An example client written in python can be found [here](https://gitlab.com/debian-pm/tools/build/-/blob/master/docker/dpmput.py).

In most cases it is necessary to override the default upload size limits.
You can do that by setting an environment variable:
```sh
ROCKET_LIMITS={file=2GB,bytes=2GB,data-form=2GB,form=2GB,string=2GB}
```

**status**: finished
